import React, { PureComponent } from 'react';
import { View,Image, StyleSheet, FlatList, TouchableOpacity, Text, ActivityIndicator } from 'react-native';

export default class Covid19 extends PureComponent {
    state = {
        covid19: [],
        covidt: [],
        loading: true,
        refreshing: false
    }
   
    async componentDidMount() {
        try { 
            const coronaApiCall = await fetch('https://api.covid19api.com/summary');
            const corona = await coronaApiCall.json();
            this.setState({covid19: corona.Countries, loading: false});
            this.setState({covidt: corona.Global, loading: false});
        } catch(err) {
            console.log("Error fetching data-----------", err);
        }
    }
    
    renderItem(data) {
        return( 
          <TouchableOpacity style={{backgroundColor: 'transparent'}}>
            <View style={styles.listItemContainer}>
              <Text style={styles.coronaItemText}>{data.item.Country}</Text>
              <Text style={styles.coronaItemText}>{data.item.TotalConfirmed}</Text>
              <Text style={styles.coronaItemText}>{data.item.TotalDeaths}</Text>
              <Text style={styles.coronaItemText}>{data.item.TotalRecovered}</Text>
            </View>
          </TouchableOpacity>
        )
    }
    
    render_FlatList_header = () => {
 
      var header_View = (
        <View style={styles.listHeadContainer}>
          <TouchableOpacity style={{flex:1}}>
            <Text style={styles.coronaItemHeader}>Country</Text>
          </TouchableOpacity>
          <TouchableOpacity style={{flex:1}}>
            <Text style={styles.coronaItemHeader}>Confirmed</Text>
          </TouchableOpacity>
          <TouchableOpacity style={{flex:1}}>
            <Text style={styles.coronaItemHeader}>Deaths</Text>
          </TouchableOpacity>
          <TouchableOpacity style={{flex:1}}>
            <Text style={styles.coronaItemHeader}>Recovered</Text>
          </TouchableOpacity>
        </View>
   
      );
   
      return header_View ;
   
    };

    render() {
        const { covid19, loading } = this.state;
        if(!loading) {
            return(
            <View>
              <View style={styles.mainrow}>
                <Text style={styles.text0}>Covid Tracker</Text>
                <TouchableOpacity>
                  <Image
                    style={styles.tinyLogo}
                    source={require('./four-lines.png')}
                  />
                </TouchableOpacity>
              </View>
              <View style={styles.row}>
                <View style={styles.total}>
                  <Text style={styles.text}>Total Cases</Text>
                  <Text style={styles.text2}>{this.state.covidt.TotalConfirmed}</Text>
                </View>
                <View style={styles.death}>
                  <Text style={styles.text}>Deaths</Text>
                  <Text style={styles.text2}>{this.state.covidt.TotalDeaths}</Text>
                </View>
                <View style={styles.recover}>
                  <Text style={styles.text}>Recovered</Text>
                  <Text style={styles.text2}>{this.state.covidt.TotalRecovered}</Text>
                </View>
              </View>
              <FlatList 
                data={covid19}
                renderItem={this.renderItem}
                keyExtractor={(item) => item.Country}
                ListHeaderComponent={this.render_FlatList_header}
              />
            </View>
            )
        } else {
            return <ActivityIndicator />
        }
    }
}
const styles = StyleSheet.create({
  mainrow:{
    flexDirection:'row',
    justifyContent:'space-between',
    marginTop:'5%',
    marginHorizontal:'4%'
  },
  tinyLogo:{

  },
  text0:{
    fontSize:24,
    color:'black',
  },
  row:{
    flexDirection:'row',
    marginTop:'5%',
    marginBottom:'3%',
  },
  total:{
    backgroundColor:'#ffb3b3',
    padding:'3%',
    borderRadius:10,
    width:'30%',
    marginLeft:'2%',
    marginRight:'1.5%',
  },
  death:{
    backgroundColor:'#813626',
    padding:'3%',
    borderRadius:10,
    width:'30%',
    marginLeft:'1.5%',
    marginRight:'1.5%',
  },
  recover:{
    backgroundColor:'#46b68b',
    padding:'3%',
    borderRadius:10,
    width:'30%',
    marginLeft:'1.5%',
    marginRight:'2%',
  },
  text:{
    fontSize:14,
    color:'black',
    textAlign:'center'
  },
  text2:{
    fontSize:18,
    color:'black',
    textAlign:'center'

  },
  listHeadContainer: {
    backgroundColor:'#46b68b',
    flexDirection: 'row',
    paddingVertical:'2%',
    borderRadius:10,
    marginHorizontal:'2%',
    marginBottom:'2%'
  },
  coronaItemHeader: {
    color: 'black',
    fontSize: 14,
    textAlign:'center',
    fontWeight:'bold',
  },
  listItemContainer: {
    flexDirection: 'row',
    alignItems:'center',
    paddingVertical:'2%',
    marginHorizontal:'2%',
  },
  coronaItemText: {
    color: 'black',
    fontSize: 14,
    flex: 1,
    textAlign:'center',
  },
});